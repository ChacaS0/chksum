/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/chacas0/go-spinner"
	"github.com/spf13/cobra"

	"gitlab.com/ChacaS0/chksum/lib"
	"gitlab.com/ChacaS0/chksum/util"
)

// sha1FilePath is the file to perform checksum on
var sha1FilePath string

// sha1Sum is the given sum we have and want to check
var sha1Sum string

// sha1Output is the path to the output file
var sha1Output string

func init() {
	rootCmd.AddCommand(sha1Cmd)

	sha1Cmd.Flags().StringVarP(&sha1FilePath, "filePath", "f", "", util.FileUsage)
	sha1Cmd.Flags().StringVarP(&sha1Sum, "sum", "s", "", util.SumUsage)
	sha1Cmd.Flags().StringVarP(&sha1Output, "output", "o", "", util.OutputUsage)
}

// sha1Cmd represents the sha1 command
var sha1Cmd = &cobra.Command{
	Use:   "sha1",
	Short: util.Sha1Short,
	Long:  util.None,
	Run: func(cmd *cobra.Command, args []string) {
		// First we check the flags
		if err := util.CheckFlags(sha1FilePath, sha1Sum, sha1Output); err != nil {
			fmt.Println(util.SDisplayErr(err, ""))
			os.Exit(1)
		}

		s := spinner.NewSpinner("Processing ...")
		s.SetCharset(util.LoadingChars)
		s.SetSpeed(util.LoadingSpeed)
		s.Start()

		hash, err := lib.ComputeSha1(sha1FilePath)
		if err != nil {
			s.Stop()
			fmt.Println(util.SDisplayErr(err, ""))
			os.Exit(1)
		}

		// If we do not provide a sum to compare to,
		// we only display the sum
		if sha1Sum == "" {
			if sha1Output != "" {
				err := ioutil.WriteFile(sha1Output, []byte(hash), 0644)
				if err != nil {
					util.SDisplayErr(err, "")
					os.Exit(1)
				}
			}
			s.Stop()
			fmt.Println(hash)
		} else {
			// Otherwise, we compare it
			comparison := &lib.Compare{
				FileHash:  strings.ToLower(hash),
				GivenHash: strings.ToLower(lib.GetSum(sha1Sum)),
			}

			// feed the struct with appropriate data
			comparison.CompareSha1()

			ln1, ln2 := comparison.ResultsWithDiffs()

			s.Stop()

			fmt.Println(ln1)
			fmt.Println(ln2)

			// Print recap only if there is no error
			if comparison.ErrorInfo == nil {
				fmt.Println(comparison.Recap())
			}
		}
	},
}
