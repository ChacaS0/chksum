/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/chacas0/go-spinner"
	"github.com/spf13/cobra"

	"gitlab.com/ChacaS0/chksum/lib"
	"gitlab.com/ChacaS0/chksum/util"
)

// md5FilePath is the file to perform checksum on
var md5FilePath string

// md5Sum is the given sum we have and want to check
var md5Sum string

// md5Output is the path to the output file
var md5Output string

func init() {
	rootCmd.AddCommand(md5Cmd)

	md5Cmd.Flags().StringVarP(&md5FilePath, "filePath", "f", "", util.FileUsage)
	md5Cmd.Flags().StringVarP(&md5Sum, "sum", "s", "", util.SumUsage)
	md5Cmd.Flags().StringVarP(&md5Output, "output", "o", "", util.OutputUsage)
}

// md5Cmd represents the md5 command
var md5Cmd = &cobra.Command{
	Use:   "md5",
	Short: util.Md5Short,
	Long:  util.None,
	Run: func(cmd *cobra.Command, args []string) {
		// First we check the flags
		if err := util.CheckFlags(md5FilePath, md5Sum, md5Output); err != nil {
			fmt.Println(util.SDisplayErr(err, ""))
			os.Exit(1)
		}

		s := spinner.NewSpinner("Processing ...")
		s.SetCharset(util.LoadingChars)
		s.SetSpeed(util.LoadingSpeed)
		s.Start()

		hash, err := lib.ComputeMd5(md5FilePath)
		if err != nil {
			s.Stop()
			fmt.Println(util.SDisplayErr(err, ""))
			os.Exit(1)
		}

		// If we do not provide a sum to compare to,
		// we only display the sum
		if md5Sum == "" {
			if md5Output != "" {
				err := ioutil.WriteFile(md5Output, []byte(hash), 0644)
				if err != nil {
					util.SDisplayErr(err, "")
					os.Exit(1)
				}
			}
			s.Stop()
			fmt.Println(hash)
		} else {
			// Otherwise, we compare it
			comparison := &lib.Compare{
				FileHash:  strings.ToLower(hash),
				GivenHash: strings.ToLower(lib.GetSum(md5Sum)),
			}

			// feed the struct with appropriate data
			comparison.CompareMd5()

			ln1, ln2 := comparison.ResultsWithDiffs()

			s.Stop()

			fmt.Println(ln1)
			fmt.Println(ln2)

			// Print recap only if there is no error
			if comparison.ErrorInfo == nil {
				fmt.Println(comparison.Recap())
			}
		}
	},
}
