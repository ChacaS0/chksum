/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/chacas0/go-spinner"
	"github.com/spf13/cobra"

	"gitlab.com/ChacaS0/chksum/lib"
	"gitlab.com/ChacaS0/chksum/util"
)

// sha512FilePath is the file to perform checksum on
var sha512FilePath string

// sha512Sum is the given sum we have and want to check
var sha512Sum string

// sha512Output is the path to the output file
var sha512Output string

func init() {
	rootCmd.AddCommand(sha512Cmd)

	sha512Cmd.Flags().StringVarP(&sha512FilePath, "filePath", "f", "", util.FileUsage)
	sha512Cmd.Flags().StringVarP(&sha512Sum, "sum", "s", "", util.SumUsage)
	sha512Cmd.Flags().StringVarP(&sha512Output, "output", "o", "", util.OutputUsage)
}

// sha512Cmd represents the sha512 command
var sha512Cmd = &cobra.Command{
	Use:   "sha512",
	Short: util.Sha512Short,
	Long:  util.None,
	Run: func(cmd *cobra.Command, args []string) {
		// First we check the flags
		if err := util.CheckFlags(sha512FilePath, sha512Sum, sha512Output); err != nil {
			fmt.Println(util.SDisplayErr(err, ""))
			os.Exit(1)
		}

		s := spinner.NewSpinner("Processing ...")
		s.SetCharset(util.LoadingChars)
		s.SetSpeed(util.LoadingSpeed)
		s.Start()

		hash, err := lib.ComputeSha512(sha512FilePath)
		if err != nil {
			s.Stop()
			fmt.Println(util.SDisplayErr(err, ""))
			os.Exit(1)
		}

		// If we do not provide a sum to compare to,
		// we only display the sum
		if sha256Sum == "" {
			if sha256Output != "" {
				err := ioutil.WriteFile(sha256Output, []byte(hash), 0644)
				if err != nil {
					util.SDisplayErr(err, "")
					os.Exit(1)
				}
			}
			s.Stop()
			fmt.Println(hash)
		} else {
			// Otherwise, we compare it
			comparison := &lib.Compare{
				FileHash:  strings.ToLower(hash),
				GivenHash: strings.ToLower(lib.GetSum(sha256Sum)),
			}

			// feed the struct with appropriate data
			comparison.CompareSha512()

			ln1, ln2 := comparison.ResultsWithDiffs()

			s.Stop()

			fmt.Println(ln1)
			fmt.Println(ln2)

			// Print recap only if there is no error
			if comparison.ErrorInfo == nil {
				fmt.Println(comparison.Recap())
			}
		}

	},
}
