/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package util

import (
	"errors"
	"time"
)

//
// * Loading spinner
//

// LoadingChars is the array of chars used
// chars are taken from {https://godoc.org/github.com/tj/go-spin#pkg-variables}.
var LoadingChars = []string{"◰", "◳", "◲", "◱"}

// LoadingSpeed defines the speed of the loading spinner.
const LoadingSpeed = time.Millisecond * 200

// Unsupported is the message to display when trying to use
// unsupported features.
const Unsupported = "Unsupported feature yet!"

//
// * Hash
//

// Sha256Len is the length of a Sha256 hash (256 bits)
const Sha256Len = 64

// Sha1Len is the length of a Sha256 hash (160 bits)
const Sha1Len = 40

// Md5Len is the length of a Sha256 hash (128 bits)
const Md5Len = 32

// Sha512Len is the length of a Sha512 hash (512 bits)
const Sha512Len = 128

//
// * FUNCS
//

// CheckFlags verifies that the required flags have been provided.
func CheckFlags(filePath, sum, output string) error {
	if filePath == "" || (output != "" && sum != "") {
		return errors.New(IncorrectFlags)
	}
	return nil
}
