/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package util

// FileUsage indicates how to use file
const FileUsage = `Full path to the file on which to perform he checksum.`

// SumUsage indicates what is the `sum` flag
const SumUsage = `Full path to the file on which to perform he checksum.`

// OutputUsage indicates what file to output to the checksum
const OutputUsage = `Full path to the output file.`

// None is and empty string. Used when therer is no usage indication.
const None = ``

// RootShort is the short description of the root command.
const RootShort = `Compare easily checksums.`

// RootLong is the long description of the root command.
const RootLong = `Computes the checksum of a file and compare it with am existing checksum.

  chksum [CHECKSUM_TYPE] -f [FILE] -s [CHECKSUM]

> Note that "-s" value can be a hash or (a file path | an URL) containing the hash only.

If the -s flag is omitted, only the checksum is computed :

  chksum [CHECKSUM_TYPE] -f [FILE]

Example : 
  chksum sha256 -f afile.f -s dd3e2cae46031e044b821f9687f67a11cdb7439187559f2d5b013b187fc1aed8

It will be displayed in green the similar elements and in red the differences.
`

// Sha256Short short usage for comand `sha256`
const Sha256Short = `Use the SHA256 checksum.`

// Sha1Short short usage for comand `sha1`
const Sha1Short = `Use the SHA1 checksum.`

// Md5Short short usage for comand `md5`
const Md5Short = `Use the MD5 checksum.`

// Sha512Short short usage for comand `sha512`
const Sha512Short = `Use the SHA512 checksum.`
