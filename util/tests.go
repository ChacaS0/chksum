/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package util

// TestSha256 is the expected sha256sum for the test
const TestSha256 = "f3a79e177332cd8a75fbf902e0cfb1f02ebf85d3eddea1ea5e5cd5867c3bdc43"

// TestSha1 is the expected sha1sum for the test
const TestSha1 = "499bf9cd4675f89f51724e3c6bba43ebf74a7649"

// TestMd5 is the expected md5sum for the test
const TestMd5 = "32f275113e8c924d1539dfc13244b34b"

// TestSha512 is the expected sha512sum for the test
const TestSha512 = "2a90ec466c2a6ef59105f7c218758190febdf5ceda5de067809d8ae3bad10c7080b7a193680535564d30c3023440d1f23bd150de06e33b6cfd9d61b6d54ac8dc"
