/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package util

import (
	"errors"
	"fmt"
	"testing"

	"github.com/fatih/color"
	"github.com/stretchr/testify/assert"
)

// TestSDisplayErr is the test for [SDisplayErr]
func TestSDisplayErr(t *testing.T) {
	testErr := errors.New("Testing errors diplay")

	// list of tests
	tests := map[string]struct {
		input1   error
		input2   string
		expected string
	}{
		"noOptMsg": {
			input1:   testErr,
			input2:   "",
			expected: color.RedString(testErr.Error()),
		},
		"wOptMsg": {
			input1:   testErr,
			input2:   "optional message",
			expected: fmt.Sprintf("%s\n\t%s", color.RedString(testErr.Error()), "optional message"),
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, SDisplayErr(test.input1, test.input2))
	}
}
