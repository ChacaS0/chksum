/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package util

import (
	"fmt"

	"github.com/fatih/color"
)

// ErrorLength indicates a difference in length.
const ErrorLength = `[ERROR] Different length.`

// IncorrectFlags indicates a incorrect set of flag when using the cmd.
const IncorrectFlags = `[ERROR] Incorrect flag`

// * FUNCS

// SDisplayErr returns a string of the error, ready to be displayed.
func SDisplayErr(err error, optMsg string) string {
	if optMsg != "" {
		return fmt.Sprintf("%s\n\t%s", color.RedString(err.Error()), optMsg)
	}
	return color.RedString(err.Error())
}
