/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package util

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestCheckFlags is the test function for [SDisplayErr].
func TestCheckFlags(t *testing.T) {

	testErr := errors.New(IncorrectFlags)

	// list of tests
	tests := map[string]struct {
		input1   string
		input2   string
		input3   string
		expected error
	}{
		"firstMissing": {
			input1:   "",
			input2:   "message",
			input3:   "message",
			expected: testErr,
		},
		"secondMising": {
			input1:   "test1",
			input2:   "",
			input3:   "message",
			expected: nil,
		},
		"thirdMising": {
			input1:   "message",
			input2:   "message",
			input3:   "",
			expected: nil,
		},
		"allMising": {
			input1:   "",
			input2:   "",
			input3:   "",
			expected: testErr,
		},
		"onlyFirst": {
			input1:   "mesage",
			input2:   "",
			input3:   "",
			expected: nil,
		},
		"all": {
			input1:   "mesage",
			input2:   "mesage",
			input3:   "mesage",
			expected: testErr,
		},
	}

	for _, test := range tests {
		err := CheckFlags(test.input1, test.input2, test.input3)
		assert.Equal(
			t, test.expected,
			err,
		)
	}
}
