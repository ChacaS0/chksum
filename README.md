# <img src="https://gitlab.com/uploads/-/system/project/avatar/13830438/chksum_logo.png?width=64" width="64px"/>
Easily compute and verify checksums validity with **chksum** (pronouced *checksum*).

<script id="asciicast-MdRgEWnnUJdZ6FczJ5F1NwHjs" src="https://asciinema.org/a/MdRgEWnnUJdZ6FczJ5F1NwHjs.js" async></script>

## Installation

### Linux

#### Archlinux

There are two versions, using the binary file or building from source:
* [`chksum-git`](https://aur.archlinux.org/packages/chksum-git/)
* [`chksum-bin-git`](https://aur.archlinux.org/packages/chksum-bin-git/)

#### Manually
*You can also clone the repository then build it from source.*

1. Download
```
curl --location --output "chksum.zip" "https://gitlab.com/chacas0/chksum/-/jobs/artifacts/master/download?job=build"
unzip chksum.zip
```
2. You can then copy it to `/usr/bin/` or put it anywhere then make a symbolic link to it or add it to your `$PATH`.

#### Windows
#### Scoop

By adding the bucket (recommended) :
```
scoop bucket add chacas0 https://gitlab.com/ChacaS0/scoop-bucket
scoop install chksum
```

Or as a one-timer :
```
scoop install https://gitlab.com/ChacaS0/chksum/-/raw/master/chksum.json
```

#### Manually

1. Download [the latest version](https://ci.appveyor.com/api/projects/chacas0/chksum/artifacts/bin/chksum.exe?branch=master).
2. Add it to the `PATH` : (with the GUI or `set PATH="Path\To\chksum";%PATH%`)


### Build from source

1. Download and install
```bash
go get -u -v gitlab.com/ChacaS0/chksum
```
2. Add `$GOBIN` or `$GOBIN/chksum` to your `PATH`.



## Usage

<a href="https://asciinema.org/a/MdRgEWnnUJdZ6FczJ5F1NwHjs"><img src="https://media.giphy.com/media/Vi0AzfCmnNUWxWfJoy/giphy.gif" style="min-width:500px" alt="chksum-basic-usage" border="0"></a>

### Checksum computation

```bash
chksum sha1 -f a_file.ext
```

* `sha1` is the algorithm and can be replaced by another *(see the help command to view the available algorithms)*
* `-f` is followed by the file's path of which we need to compute the checksum.

### Checksum comparison

```bash
chksum sha1 -f a_file.ext -s a30b07d49c48f8f566a392e11e88f8c00bb14e38
```

* `-s` is followed by the checksum

## Commands and Flags

### Commands

Commands are used to indicate which hash algorithm should be used. The available algorithms are :
* `sha1`
* `sha256`
* `sha512`
* `md5`

### Flags

Flags are used to customize the usage.

| Short | Long           | Description                                                      |
|:-----:|:--------------:|:----------------------------------------------------------------:|
|`-f`   | `--filePath`   | It is the file path for which we want to calculate the checksum. |
|`-o`   | `--output`     | It is the file path to the hash output file.                     |
|`-s`   | `--sum     `   | The checksum with which to compare. (Direct hash, file path and URL containing only the hash are accepted) |
|`-h`   | `--help`       | Display help.                                                    |
