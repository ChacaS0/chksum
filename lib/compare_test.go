/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package lib

import (
	"errors"
	"fmt"
	"testing"

	"github.com/fatih/color"
	"github.com/stretchr/testify/assert"
)

func TestRecap(t *testing.T) {
	const hash1 = "dd3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed8"
	const hash2 = "ad3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed8"

	successPrefix := "\n\u2713"
	failurePrefix := "\n\u2717"

	// list of tests
	tests := map[string]struct {
		input1   string
		input2   string
		expected string
	}{
		"hasOneDiff": {
			input1:   hash1,
			input2:   hash2,
			expected: color.RedString(fmt.Sprintf("%s 1 Diffs.", failurePrefix)),
		},
		"hasNoDiffs": {
			input1:   hash1,
			input2:   hash1,
			expected: color.GreenString(fmt.Sprintf("%s 0 Diffs.", successPrefix)),
		},
	}

	for _, test := range tests {
		c := Compare{
			FileHash:  test.input1,
			GivenHash: test.input2,
		}

		// this will fill c.PosOfDiff
		c.CompareSha256()

		assert.Equal(t, test.expected, c.Recap())
	}
}

// TestResultsWithDiffs check that we get the right result to display.
func TestResultsWithDiffs(t *testing.T) {
	const hash1 = "dd3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed8"
	const hash2 = "ad3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed8"

	anError := errors.New("Test error")

	// list of tests
	tests := map[string]struct {
		input1    string
		input2    string
		expected1 string
		expected2 string
		errInfo   error
	}{
		"hasNoDiffs": {
			input1:    hash1,
			input2:    hash1,
			expected1: fmt.Sprintf(`File checksum  : %s`, color.GreenString(hash1)),
			expected2: fmt.Sprintf(`Given checksum : %s`, color.GreenString(hash1)),
			errInfo:   nil,
		},
		"hasDiffs": {
			input1:    hash1,
			input2:    hash2,
			expected1: fmt.Sprintf(`File checksum  : %s%s`, color.RedString(string(hash1[0])), color.GreenString(string(hash1[1:]))),
			expected2: fmt.Sprintf(`Given checksum : %s%s`, color.RedString(string(hash2[0])), color.GreenString(string(hash2[1:]))),
			errInfo:   nil,
		},
		"hasError": {
			input1:    hash1,
			input2:    hash2,
			expected1: anError.Error(),
			expected2: "",
			errInfo:   anError,
		},
	}

	for _, test := range tests {
		c := Compare{
			FileHash:  test.input1,
			GivenHash: test.input2,
			ErrorInfo: test.errInfo,
		}

		// this will fill c.PosOfDiff
		c.CompareSha256()

		line1, line2 := c.ResultsWithDiffs()

		assert.Equal(t, test.expected1, line1)
		assert.Equal(t, test.expected2, line2)
		assert.Equal(t, test.errInfo, c.ErrorInfo)

	}
}

// TestIsDiff tests if two characters are differents on
// a given position.
func TestIsDiff(t *testing.T) {
	const hash1 = "dd3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed8"
	const hash2 = "aa3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed8"

	// list of tests
	tests := map[string]struct {
		input1   string
		input2   string
		index    int
		expected bool
	}{
		"false": {
			input1:   hash1,
			input2:   hash2,
			index:    2,
			expected: false,
		},
		"true": {
			input1:   hash1,
			input2:   hash2,
			index:    0,
			expected: true,
		},
	}

	for _, test := range tests {
		c := Compare{
			FileHash:  test.input1,
			GivenHash: test.input2,
		}

		// this will fill c.PosOfDiff
		c.CompareSha256()

		assert.Equal(t, test.expected, c.isDiff(test.index))
	}
}
