/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package lib

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ChacaS0/chksum/util"
)

// TestCompute512 tests [ComputeSha512]
// TODO
func TestComputeSha512(t *testing.T) {
	// Create our Temp File: This will create a filename like /tmp/prefix-123456
	// We can use a pattern of "pre-*.txt" to get an extension like: /tmp/pre-123456.txt
	// @note inspired by {https://golangcode.com/creating-temp-files/}
	tmpFile, err := ioutil.TempFile(os.TempDir(), "chksum-")
	if err != nil {
		t.Fatal("Cannot create temporary file", err)
	}
	tmpFile.WriteString("chksum sha512")

	// Remember to clean up the file afterwards
	defer os.Remove(tmpFile.Name())

	tests := map[string]struct {
		path     string
		givenSum string
		err      error
	}{ //                  path         , givenSum       , error
		"successful":     {tmpFile.Name(), util.TestSha512, nil},
		"errorDirectory": {os.TempDir(), util.TestSha512, errors.New("Path error")},
	}

	for _, test := range tests {
		hash, err := ComputeSha512(test.path)
		if test.err != nil && err != nil {
			// in case of errors
			assert.True(t, true)
			assert.Equal(t, "", hash)
		} else {
			// check for the checksum
			assert.Equal(t, test.givenSum, hash)
			assert.Equal(t, test.err, err)
		}

	}

	tmpFile.Close()
}

// TestCompareSha512 Tests the comparisons between two hash strings
func TestCompareSha512(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		input1  string
		input2  string
		nbDiffs []int
		err     error
	}{
		"successful": {
			input1:  "18a0bd36c51a5b098120b01483ab8a93597056a1660c651a844ab1b155d56a62a26e7d6e460ee92c55be9c71dbb2ecaf8976803940288bf1888fa593053ed221",
			input2:  "18a0bd36c51a5b098120b01483ab8a93597056a1660c651a844ab1b155d56a62a26e7d6e460ee92c55be9c71dbb2ecaf8976803940288bf1888fa593053ed221",
			nbDiffs: nil,
			err:     nil,
		},
		"has2Diffs": {
			input1:  "c8a0bd36c51a5b098120b01483ab8a93597056a1660c651a844ab1b155d56a62a26e7d6e460ee92c55be9c71dbb2ecaf8976803940288bf1888fa593053ed220",
			input2:  "18a0bd36c51a5b098120b01483ab8a93597056a1660c651a844ab1b155d56a62a26e7d6e460ee92c55be9c71dbb2ecaf8976803940288bf1888fa593053ed221",
			nbDiffs: []int{0, 127},
			err:     nil,
		},
		"diffOfLen1": {
			input1:  "dd3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed",
			input2:  "18a0bd36c51a5b098120b01483ab8a93597056a1660c651a844ab1b155d56a62a26e7d6e460ee92c55be9c71dbb2ecaf8976803940288bf1888fa593053ed221",
			nbDiffs: nil,
			err:     errors.New(util.ErrorLength),
		},
		"diffOfLen2": {
			input1:  "18a0bd36c51a5b098120b01483ab8a93597056a1660c651a844ab1b155d56a62a26e7d6e460ee92c55be9c71dbb2ecaf8976803940288bf1888fa593053ed221",
			input2:  "dd3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed8",
			nbDiffs: nil,
			err:     errors.New(util.ErrorLength),
		},
	}

	for _, test := range tests {
		c := Compare{
			FileHash:  test.input1,
			GivenHash: test.input2,
		}
		c.CompareSha512()
		// check diffs
		assert.Equal(t, test.nbDiffs, c.PosOfDiff)
		// check errors
		fmt.Println(c.ErrorInfo) // DEBUG
		if test.err != nil {
			assert.EqualError(t, c.ErrorInfo, util.ErrorLength)
		}
	}
}
