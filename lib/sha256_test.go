/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package lib

import (
	"errors"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ChacaS0/chksum/util"
)

// TestComputeSha256 tests [ComputeSha256]
func TestComputeSha256(t *testing.T) {
	// Create our Temp File: This will create a filename like /tmp/prefix-123456
	// We can use a pattern of "pre-*.txt" to get an extension like: /tmp/pre-123456.txt
	// @note inspired by {https://golangcode.com/creating-temp-files/}
	tmpFile, err := ioutil.TempFile(os.TempDir(), "chksum-")
	if err != nil {
		t.Fatal("Cannot create temporary file", err)
	}
	tmpFile.WriteString("chksum sha256")

	// Remember to clean up the file afterwards
	defer os.Remove(tmpFile.Name())

	tests := map[string]struct {
		path     string
		givenSum string
		err      error
	}{
		"successful":     {tmpFile.Name(), util.TestSha256, nil},
		"errorDirectory": {os.TempDir(), util.TestSha256, errors.New("Path error")},
	}

	for _, test := range tests {
		hash, err := ComputeSha256(test.path)
		if test.err != nil && err != nil {
			// in case of errors
			assert.True(t, true)
			assert.Equal(t, "", hash)
		} else {
			// check for the checksum
			assert.Equal(t, test.givenSum, hash)
			assert.Equal(t, test.err, err)
		}

	}
}

// TestCompareSha256 Tests the comparisons between two hash strings
func TestCompareSha256(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		input1  string
		input2  string
		nbDiffs []int
		err     error
	}{
		"successful": {
			input1:  "dd3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed8",
			input2:  "dd3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed8",
			nbDiffs: nil,
			err:     nil,
		},
		"has2Diffs": {
			input1:  "dd3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed8",
			input2:  "ab3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed8",
			nbDiffs: []int{0, 1},
			err:     nil,
		},
		"diffOfLen": {
			input1:  "dd3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed8",
			input2:  "3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed8",
			nbDiffs: nil,
			err:     errors.New(util.ErrorLength),
		},
	}

	for _, test := range tests {
		c := Compare{
			FileHash:  test.input1,
			GivenHash: test.input2,
		}
		c.CompareSha256()
		// check diffs
		assert.Equal(t, test.nbDiffs, c.PosOfDiff)
		// check errors
		assert.Equal(t, test.err, c.ErrorInfo)
	}
}
