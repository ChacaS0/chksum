/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package lib

import (
	"errors"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ChacaS0/chksum/util"
)

// TestComputeMd5 tests [ComputeMd5]
func TestComputeMd5(t *testing.T) {
	// Create our Temp File: This will create a filename like /tmp/prefix-123456
	// We can use a pattern of "pre-*.txt" to get an extension like: /tmp/pre-123456.txt
	// @note inspired by {https://golangcode.com/creating-temp-files/}
	tmpFile, err := ioutil.TempFile(os.TempDir(), "chksum-")
	if err != nil {
		t.Fatal("Cannot create temporary file", err)
	}
	tmpFile.WriteString("chksum Md5")

	// Remember to clean up the file afterwards
	defer os.Remove(tmpFile.Name())

	tests := map[string]struct {
		path     string
		givenSum string
		err      error
	}{
		"successful":     {tmpFile.Name(), util.TestMd5, nil},
		"errorDirectory": {os.TempDir(), util.TestMd5, errors.New("Path error")},
	}

	for _, test := range tests {
		hash, err := ComputeMd5(test.path)
		if test.err != nil && err != nil {
			// in case of errors
			assert.True(t, true)
			assert.Equal(t, "", hash)
		} else {
			// check for the checksum
			assert.Equal(t, test.givenSum, hash)
			assert.Equal(t, test.err, err)
		}

	}
}

// TestCompareMd5 Tests the comparisons between two hash strings
func TestCompareMd5(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		input1  string
		input2  string
		nbDiffs []int
		err     error
	}{
		"successful": {
			input1:  "bf91dc22b2c18109bbd48111d818339b",
			input2:  "bf91dc22b2c18109bbd48111d818339b",
			nbDiffs: nil,
			err:     nil,
		},
		"has2Diffs": {
			input1:  "bf91dc22b2c18109bbd48111d818339b",
			input2:  "c091dc22b2c18109bbd48111d818339b",
			nbDiffs: []int{0, 1},
			err:     nil,
		},
		"diffOfLen": {
			input1:  "bf91dc22b2c18109bbd48111d818339b",
			input2:  "3e1cae46041e044b821f9697f67a11cdbf",
			nbDiffs: nil,
			err:     errors.New(util.ErrorLength),
		},
	}

	for _, test := range tests {
		c := Compare{
			FileHash:  test.input1,
			GivenHash: test.input2,
		}
		c.CompareMd5()
		// check diffs
		assert.Equal(t, test.nbDiffs, c.PosOfDiff)
		// check errors
		assert.Equal(t, test.err, c.ErrorInfo)
	}
}
