/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package lib

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
)

// GetSum will return just the checksum for the input.
// Aimed to support :
// - `<string>` use the direct input
// - `<URL>`    use directly the URL (the checksum should be the only content displayed)
// - `<path>`   use a path to point to a text file (.txt)
func GetSum(flagInput string) string {
	// * FILE
	if output := getSumFromFile(flagInput); output != "" {
		return output
	}

	// * URL
	if isValidURL(flagInput) {
		// if error then it may not be an URL
		resp, err := http.Get(flagInput)
		if err == nil {
			defer resp.Body.Close()

			// read content
			html, _ := ioutil.ReadAll(resp.Body)

			// return the HTML content as a string %s
			return fmt.Sprintf("%s", html)
		}
	}

	// * DIRECT HASH
	return flagInput
}

// getSumFromFile reads a file and extract the hash from it.
// The file must contain the hash ONLY.
// If the input is not a file, an empty string will be returned.
func getSumFromFile(input string) string {
	if data, err := ioutil.ReadFile(input); err == nil {
		return string(data)
	}
	return ""
}

// isValidURL tests a string to determine if it is a url or not.
// FTP|SFTP are forbidden.
func isValidURL(toTest string) bool {
	_, err := url.Parse(toTest)
	if err != nil {
		return false
	}

	// reject ftp, sftp and others ...
	const regForbidden = `^(ftp|sftp):\/\/`
	if regexp.MustCompile(regForbidden).MatchString(toTest) {
		return false
	}

	// only allow HTTP or HTTPS (default http)
	const regex = `(?i)^((((http|https)\:\/{2})((([a-z0-9]|-?)+\.){0,1}){0,1})|((([a-z0-9]|\-?[a-z0-9])+\.){0,}))(([a-z0-9]|([a-z0-9]+\-[a-z0-9]+))+\.){1,}([a-z]{1,})((\/|\?).*)*$`
	re := regexp.MustCompile(regex)
	if re.MatchString(toTest) {
		return true
	}

	return false
}
