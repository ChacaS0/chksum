/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package lib

import (
	"fmt"
	"strings"

	"github.com/fatih/color"
)

// Compare is used to compare two hashes
// The speciffic comparisons methods are in their dedicated
// file (e.g. "lib/sha256.go")
type Compare struct {
	// FileHash is the file's hash already computed
	FileHash string

	// GivenHash is the hash we have and want to use
	GivenHash string

	// PosOfDiff gives the positions (indexes) of where
	// are the differences between the two hashes
	PosOfDiff []int

	// ErrorInfo gives some more info about errors and such.
	// This is mostly used to indicated a difference of length
	// between the two hashes.
	ErrorInfo error

	// HashLen simply indicates how long is the hash (set after the diff computation)
	HashLen int
}

// ResultsWithDiffs gives the two strings with the differences highlighted
func (c *Compare) ResultsWithDiffs() (firstLine, secondLine string) {
	// if errors
	if c.ErrorInfo != nil {
		firstLine = c.ErrorInfo.Error()
		return
	}

	var line1 strings.Builder
	line1.WriteString(`File checksum  : `)

	var line2 strings.Builder
	line2.WriteString(`Given checksum : `)

	for i := 0; i < c.HashLen; i++ {
		if c.isDiff(i) {
			// red
			line1.WriteString(color.RedString(fmt.Sprintf("%s", string(c.FileHash[i]))))
			line2.WriteString(color.RedString(fmt.Sprintf("%s", string(c.GivenHash[i]))))
		} else {
			// green
			line1.WriteString(color.GreenString(fmt.Sprintf("%s", string(c.FileHash[i]))))
			line2.WriteString(color.GreenString(fmt.Sprintf("%s", string(c.GivenHash[i]))))
		}
	}

	return line1.String(), line2.String()
}

// isDiff returns true if the `i`th element is different between
// the computed hash and the given one.
func (c *Compare) isDiff(i int) bool {
	for _, di := range c.PosOfDiff {
		if i == di {
			return true
		}
	}
	return false
}

// Recap gives the recap message about the differences spotted.
func (c *Compare) Recap() string {
	recap := color.GreenString("\n\u2713 0 Diffs.")

	nbDiffs := len(c.PosOfDiff)

	if nbDiffs > 0 {
		// there are diffs : red
		recap = color.RedString(fmt.Sprintf("\n\u2717 %v Diffs.", nbDiffs))
	}

	return recap
}
