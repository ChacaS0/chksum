/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package lib

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"io"
	"os"

	"gitlab.com/ChacaS0/chksum/util"
)

// ComputeSha256 computes the sha256 of a file(/directory)
// and gives back only the hash as a string.
// @link {https://golang.org/pkg/crypto/sha256/#New}
func ComputeSha256(fullPath string) (string, error) {
	f, err := os.Open(fullPath)
	if err != nil {
		return "", err
	}
	defer f.Close()

	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", h.Sum(nil)), nil
}

// CompareSha256 will feed the [Compare] on which it is called
// to give differences between the two hashes *specific to sha256 hashes*.
func (c *Compare) CompareSha256() {
	// check lengths
	c.HashLen = len(c.FileHash)
	if c.HashLen != len(c.GivenHash) || c.HashLen != util.Sha256Len {
		c.ErrorInfo = errors.New(util.ErrorLength)
		return
	}

	// set the differences
	for i := 0; i < len(c.GivenHash); i++ {
		if c.FileHash[i] != c.GivenHash[i] {
			c.PosOfDiff = append(c.PosOfDiff, i)
		}
	}
}
