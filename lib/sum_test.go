/*
Copyright © 2019 ChacaS0 <incoming+chacas0-chksum-13830438-issue-@incoming.gitlab.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package lib

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestGetSum tests [GetSum]
func TestGetSum(t *testing.T) {
	const aHash = "dd3e1cae46041e044b821f9697f67a11cda7439187559f2d5b013b187fc1aed8"

	tmpFile, err := ioutil.TempFile(os.TempDir(), "")
	if err != nil {
		t.Fatal("Cannot create temporary file", err)
	}
	defer os.Remove(tmpFile.Name())

	tmpFile.WriteString(aHash)

	// list of tests
	tests := map[string]struct {
		input    string
		expected string
	}{
		"direct_hash": {
			input:    aHash,
			expected: aHash,
		},
		"file_path": {
			input:    tmpFile.Name(),
			expected: aHash,
		},
		"url_1": {
			input:    "https://gitlab.com/ChacaS0/chksum/raw/master/res/tests/sha256_good_main.txt",
			expected: aHash,
		},
	}

	// actual testing
	for _, test := range tests {
		assert.Equal(t, GetSum(test.input), test.expected)
	}
}

// TestIsURL test for [isURL]
// inspired from {https://github.com/asaskevich/govalidator}
func TestIsURL(t *testing.T) {
	t.Parallel()

	var tests = []struct {
		param    string
		expected bool
	}{
		{"", false},
		{"http://foo.bar#com", false},
		{"http://foobar.com", true},
		{"https://foobar.com", true},
		{"foobar.com", true},
		{"http://foobar.coffee/", true},
		{"http://foobar.中文网/", false},
		{"http://foobar.org/", true},
		{"http://foobar.ORG", true},
		// {"http://foobar.org:8080/", true}, // personalised port not supported for now
		{"ftp://foobar.ru/", false},
		{"sftp://foobar.ru/", false},
		{"ftp.foo.bar", true},
		{"http://user:pass@www.foobar.com/", false},          // not supported for now
		{"http://user:pass@www.foobar.com/path/file", false}, // not supported for now
		{"http://127.0.0.1/", false},                         // not supported for now
		{"http://duckduckgo.com/?q=%2F", true},
		{"http://localhost:3000/", false}, // not supported for now
		{"http://foobar.com/?foo=bar#baz=qux", true},
		{"http://foobar.com?foo=bar", true},
		{"http://www.xn--froschgrn-x9a.net/", false},
		{"http://foobar.com/a-", true},
		{"http://foobar.پاکستان/", false},
		{"http://foobar.c_o_m", false},
		{"http://_foobar.com", false},
		{"http://foo_bar.com", false},
		{"http://user:pass@foo_bar_bar.bar_foo.com", false}, // not supported for now
		{"xyz://foobar.com", false},
		{".com", false},
		{"rtmp://foobar.com", false},
		{"http://localhost:3000/", false},    // not supported for now
		{"http://foobar.com#baz=qux", false}, // not supported for now
		{"http://r6---snnvoxuioq6.googlevideo.com", true},
		{"someone@example.com", false},
		{"irc://irc.server.org/channel", false},
		{"irc://#channel@network", false},
		{"/abs/test/dir", false},
		{"./rel/test/dir", false},
		{"http://foo^bar.org", false},
		{"http://foo&*bar.org", false},
		{"http://foo&bar.org", false},
		{"http://foo bar.org", false},
		{"http://foo.bar.org", true},
		{"http://www.foo.bar.org", true},
		{"http://www.foo.co.uk", true},
		{"foo", false},
		// {"http://.foo.com", false},
		{"http://,foo.com", false},
		{",foo.com", false},
		{"http://myservice.:9093/", false}, // not supported for now
		{"https://pbs.twimg.com/profile_images/560826135676588032/j8fWrmYY_normal.jpeg", true},
		// {"http://prometheus-alertmanager.service.q:9093", true},
		{"aio1_alertmanager_container-63376c45:9093", false}, // not supported for now
		// {"https://www.logn-123-123.url.with.sigle.letter.d:12345/url/path/foo?bar=zzz#user", true},
		{"http://me.example.com", true},
		{"http://www.me.example.com", true},
		{"https://farm6.static.flickr.com", true},
		{"https://zh.wikipedia.org/wiki/Wikipedia:%E9%A6%96%E9%A1%B5", true},
		{"google", false},
		// According to #87
		{"http://hyphenated-host-name.example.co.in", true},
		// {"http://cant-end-with-hyphen-.example.com", false},
		// {"http://-cant-start-with-hyphen.example.com", false},
		{"http://www.domain-can-have-dashes.com", true},
		{"http://m.abcd.com/test.html", true},
		{"http://m.abcd.com/a/b/c/d/test.html?args=a&b=c", true},
		{"http://[::1]:9093", false},
		{"http://[::1]:909388", false},
		{"1200::AB00:1234::2552:7777:1313", false},
		{"http://[2001:db8:a0b:12f0::1]/index.html", false},
		{"http://[1200:0000:AB00:1234:0000:2552:7777:1313]", false},
		{"http://user:pass@[::1]:9093/a/b/c/?a=v#abc", false},
		{"https://127.0.0.1/a/b/c?a=v&c=11d", false},
		{"https://foo_bar.example.com", false},
		{"http://cant_hanve_underscore.example.com", false},
		{"http://foo_bar_fizz_buzz.example.com", false},
		{"http://_cant_start_with_underescore", false},
		{"http://cant_end_with_underescore_", false},
	}
	for _, test := range tests {
		actual := isValidURL(test.param)
		if actual != test.expected {
			t.Errorf("Expected IsURL(%q) to be %v, got %v", test.param, test.expected, actual)
		}
	}
}
